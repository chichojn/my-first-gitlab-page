<a name="ReturnCode" id="returncode"></a>
# Return Code

<a name="Common" id="common"></a>
## 1. Common
| Code |<div style="width:330px">Message</div> |
| --- | --- | 
|200000 | Success |  
|400000	|Parameters error message|
|401001	|Merchant signature invalid|
|401003	|Merchant biz access not authorized|
|401006	|IP restricted|
|403000	|Invalid card info|
|403500	|Suspicious transaction|
|404002	|Merchant not found|
|404003	|Payment order not found   |    
|404005	|Merchant config not found|
|404007	|Merchant biz config not found|
|404009	|Merchant config info not found|
|409001	|Merchant status invalid|
|429000	|Too many requests|
|500001	|Internal server error|
|503001	|Service unavailable|
|504001	|Service timeout|

<a name="Payment" id="payment"></a>
## 2. Payment
| Code |<div style="width:330px">Message</div> |
| --- | --- | 
|401008 | Illegal amount | 
|403001	|Expired card |                          
|403002	|Amount exceed|                  
|403003	|Invalid transaction|                
|403004	|Authentication failed    |             
|403005	|Card type not supported|  
|404008	|Card info not found  |                 
|408001	|Session timeout|
|409002	|The order status don't support payment|
|422009	|Payment re-submit|
|429001	|Payment re-submit      |     
|429003	|The payment data is inconsistent|
|429006	|Payment succeed|
|430001	|Payer not exist|
|430002	|Payer invalid|
|430003	|Payer balance not enough|
|430004	|Barcode invalid|
|430005	|Barcode expired|
|430006	|payer mismatch|
|430007	|card not support for payment|

<a name="Cancel" id="cancel"></a>
## 3. Cancel
| Code |<div style="width:330px">Message</div> |
| --- | --- | 
 422018 | Cancel not allowed due to payment success | 
|422019 | Cancel not allowed due to payment is in progress | 

<a name="Refund" id="refund"></a>
## 4. Refund
| Code |<div style="width:330px">Message</div> |
| --- | --- | 
|404004 | Merchant account not found | 
|404006	|Refund order not found|
|409002	|The order status don't support refund|
|409005	|Refund currency not in accordance with the payment|
|422001	|Refund amount exceed payment amount|
|422002	|Merchant account balance not enough|
|422003	|Refund re-submit|
|422004	|Refund expired|
|422006	|Not support partial refund|
|422007	|Not support UnionPay refund|
|422010	|Refund not allowed due to payment not success|