# Shopify Payment by Lianlian Pay Guideline

<b>Step 1</b>: Submit merchant background documents and required access parameters to Lianlian Pay.
Parameters are as followings: 
<ul><ul> ✓ Domain name (Store URL), e.g https://test.myshopify.com/</ul></ul>
<ul><ul> ✓ IP whitelist </ul></ul>
<b>Step 2</b>: Go through merchant onboard process

<b>Step 3</b>: Once merchant onboard is successfully, <u><b>Merchant ID</b></u> and <u><b>Secret key</b></u> will be sent to your
email

<b>Step 4</b>: Store Currency checking
>Before you go to payment setting, currently, only <b>THB</b> is available for Lianlian Pay to accept payments in Thailand.<br> Please make sure your store currency already set to be THB.

<div align="center">
<img width="800" src="./images/shopify/step4.PNG"/>
</div>


<b>Step 5</b>: Configuration on Shopify
<ul><ul> ✓ Go to Settings. Find “Payments” and click to continue</ul></ul>
<div align="center">
<img width="800" src="./images/shopify/step5.PNG"/>
</div>

<ul><ul> ✓ In the Third-party providers section, click “Choose third-party provider”</ul></ul>
<div align="center">
<img width="800" src="./images/shopify/step5.1.PNG"/>
</div>

<ul><ul> ✓ Choose Lianlian Pay and continue</ul></ul>
<div align="center">
<img width="800" src="./images/shopify/step5.2.PNG"/>
</div>

<ul><ul> ✓ Enter the <u><b>Merchant ID</b></u> and <u><b>Secret key</b></u> you received</ul></ul>
<div align="center">
<img width="800" src="./images/shopify/step5.3.PNG"/>
</div>

<ul><ul> ✓ Select <u><b>Visa, Mastercard, JCB</b></u> as payment methods, and submit. (<u>Note: American Express, Discover, and Diners Club is not available yet</u>) </ul></ul>
<div align="center">
<img width="800" src="./images/shopify/step5.4.PNG"/>
</div>

<ul><ul> ✓ Done! Lianlian Pay is now activated. </ul></ul>
<div align="center">
<img width="800" src="./images/shopify/step5.5.PNG"/>
</div>