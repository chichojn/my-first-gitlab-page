<!-- docs/_sidebar.md -->

* Guides
  * [Get Started](/)

* Plugin
  * [Shopify](/plugin/shopify/index)
  * [Woocommerce](/plugin/woocommerce/index)
* Payment Methods
	* [Alipay (Online)](/payment-methods/alipay-online/index)
	* [Alipay (In-Store)](/payment-methods/alipay-qr/index)
	* [Alipay (Online) SDK](/payment-methods/sdk-alipay-online/index)
	* [WeChat Pay](/payment-methods/wechat/index)
	* [QR PromptPay](/payment-methods/thai-qr/index)
	* [Credit/Debit Card](/payment-methods/bankcard/index)
* Reference
  * [Currency](/reference/currency)
  * [Order status](/reference/order-status)
  * [Refund status](/reference/refund-status)
  * [Return Code](/reference/return-code)
